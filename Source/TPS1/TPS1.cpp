// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TPS1.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TPS1, "TPS1" );

DEFINE_LOG_CATEGORY(LogTPS1)
 