// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TPS1GameMode.h"
#include "TPS1PlayerController.h"
#include "TPS1Character.h"
#include "UObject/ConstructorHelpers.h"

ATPS1GameMode::ATPS1GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATPS1PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}